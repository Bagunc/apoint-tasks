export default [
  {
    href: '/',
    value: 'home',
    label: 'לוח בקרה'
  },
  {
    href: '/processes',
    value: 'processes',
    label: 'תהליכים'
  },
  {
    href: '/filtering',
    value: 'filtering',
    label: 'סינון מורחב'
  }
]
