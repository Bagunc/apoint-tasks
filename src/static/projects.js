import { createRandomDate } from '@/utils.js'

const P__ID__1 = Math.random().toString(36).substr(2)
const P__ID__2 = Math.random().toString(36).substr(2)
const P__ID__3 = Math.random().toString(36).substr(2)

const P__PID__1 = Math.random().toString(36).substr(2)
const P__PID__2 = Math.random().toString(36).substr(2)
const P__PID__3 = Math.random().toString(36).substr(2)
const P__PID__4 = Math.random().toString(36).substr(2)
const P__PID__5 = Math.random().toString(36).substr(2)

export default [
  {
    id: P__ID__1,
    name: 'פריסת דפי HTML',
    processes: [
      {
        id: P__PID__1,
        projectId: P__ID__1,
        name: 'ייבא משאבים',
        tasks: [
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 2,
            subject: 'גופנים וסמלים',
            charge: 'underContract',
            status: 'during',
            projectId: P__ID__1,
            processId: P__PID__1,
            date: {
              start: createRandomDate(),
              finish: createRandomDate(),
              approvalExecution: createRandomDate(),
              complation: createRandomDate(),
              confirmation: createRandomDate(),
              allegedEnd: createRandomDate(),
              actualEnd: createRandomDate()
            }
          }
        ]
      },
      {
        id: P__PID__2,
        projectId: P__ID__1,
        name: 'יצירת כל רכיבי ממשק המשתמש',
        tasks: [
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 5,
            subject: 'מגיב',
            charge: 'actualHours',
            status: 'pending',
            projectId: P__ID__1,
            processId: P__PID__2,
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              approvalExecution: createRandomDate(),
              complation: createRandomDate(),
              confirmation: createRandomDate(),
              actualEnd: createRandomDate()
            }
          },
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 10,
            subject: 'פריסת כל הרכיבים',
            charge: 'actualHours',
            status: 'completed',
            projectId: P__ID__1,
            processId: P__PID__2,
            date: {
              start: createRandomDate(),
              allegedEnd: createRandomDate(),
              finish: createRandomDate(),
              approvalExecution: createRandomDate(),
              complation: createRandomDate(),
              confirmation: createRandomDate(),
              actualEnd: createRandomDate()
            }
          },
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 3,
            projectId: P__ID__1,
            processId: P__PID__2,
            subject: 'מודלים וטיפ כלים',
            charge: 'fixedMonthlyFee',
            status: 'unfinished',
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              approvalExecution: createRandomDate(),
              complation: createRandomDate(),
              actualEnd: createRandomDate()
            }
          }
        ]
      }
    ]
  },
  {
    id: P__ID__2,
    name: 'פריסת מתיחה על laravel',
    processes: [
      {
        id: P__PID__3,
        projectId: P__ID__2,
        name: 'קידוד PHP',
        tasks: [
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 2,
            projectId: P__ID__2,
            processId: P__PID__3,
            subject: 'פונקציונליות תחתונה וסרגל צד',
            charge: 'underContract',
            status: 'unfinished',
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              complation: createRandomDate(),
              approvalExecution: createRandomDate(),
              actualEnd: createRandomDate()
            }
          },
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 4,
            projectId: P__ID__2,
            processId: P__PID__3,
            subject: 'פונקציונליות כותרת',
            charge: 'underContract',
            status: 'pending',
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              complation: createRandomDate(),
              confirmation: createRandomDate(),
              approvalExecution: createRandomDate(),
              actualEnd: createRandomDate()
            }
          },
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 5,
            projectId: P__ID__2,
            processId: P__PID__3,
            subject: 'פונקציונליות תוכן',
            charge: 'underContract',
            status: 'during',
            date: {
              start: createRandomDate(),
              allegedEnd: createRandomDate(),
              finish: createRandomDate(),
              complation: createRandomDate(),
              confirmation: createRandomDate(),
              approvalExecution: createRandomDate(),
              actualEnd: createRandomDate()
            }
          }
        ]
      }
    ],
  },
  {
    id: P__ID__3,
    name: 'חנות אינטרנט all.com',
    processes: [
      {
        id: P__PID__4,
        projectId: P__ID__3,
        name: 'צומת JS קידוד',
        tasks: [
          {
            id: Math.random().toString(36).substr(2),
            projectId: P__ID__3,
            processId: P__PID__4,
            estimatingHours: 6,
            subject: 'אימות משתמש',
            charge: 'actualHours',
            status: 'completed',
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              complation: createRandomDate(),
              approvalExecution: createRandomDate(),
              actualEnd: createRandomDate()
            }
          },
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 20,
            projectId: P__ID__3,
            processId: P__PID__4,
            subject: 'עגלת קניות וקופה',
            charge: 'actualHours',
            status: 'during',
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              complation: createRandomDate(),
              approvalExecution: createRandomDate(),
              confirmation: createRandomDate()
            }
          },
        ]
      },
      {
        id: P__PID__5,
        projectId: P__ID__3,
        name: 'שירותי חיבור',
        tasks: [
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 25,
            projectId: P__ID__3,
            processId: P__PID__5,
            subject: 'שילוב שירותי המסירה',
            charge: 'fixedMonthlyFee',
            status: 'unfinished',
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              approvalExecution: createRandomDate(),
              complation: createRandomDate(),
              actualEnd: createRandomDate()
            }
          },
          {
            id: Math.random().toString(36).substr(2),
            estimatingHours: 25,
            projectId: P__ID__3,
            processId: P__PID__5,
            subject: 'שילוב שירותי התשלום',
            charge: 'fixedMonthlyFee',
            status: 'unfinished',
            date: {
              allegedEnd: createRandomDate(),
              start: createRandomDate(),
              finish: createRandomDate(),
              approvalExecution: createRandomDate(),
              complation: createRandomDate(),
              actualEnd: createRandomDate()
            }
          }
        ]
      }
    ]
  }
]