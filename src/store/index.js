import { createStore } from 'vuex'

import task from '@/store/modules/task'
import filter from '@/store/modules/filter'
import project from '@/store/modules/project'

import PROJECTS from '@/static/projects'

export default createStore({
  state: {
    statuses: {
      during: 'משרות בעבודה',
      pending: 'משימות ממתינות',
      completed: 'משימות שהושלמו',
      unfinished: 'משימות לא גמורות'
    },
    charges: {
      underContract: 'תחת חוזה',
      actualHours: 'לפי שעות בפועל',
      fixedMonthlyFee: 'תשלום חודשי קבוע'
    },
    dates: {
      production: 'תאריך יצירה',
      finish: 'תאריך סיום משימה ',
      allegedEnd: 'תאריך סיום בפועל',
      confirmation: 'אישור קבלה ',
      еstimatedEnd: 'תאריך סיום משוער',
      approvalForExecution: 'תאריך אישור לביצוע'
    },
    projects: [],
    expendBlocks: true,
    addTaskVisible: false,
  },
  mutations: {
    setProjects: (state, payload) => state.projects = payload,
    setExpended: (state, payload) => state.expendBlocks = payload,
    setAddTaskVisible: (state, payload) => state.addTaskVisible = payload,
  },
  getters: {
    getProjects: state => state.projects,
    getCharges: state => state.charges,
    getStatuses: state => state.statuses,
    getExpendBlocks: state => state.expendBlocks,
    getAddTaskVisible: state => state.addTaskVisible,
    getChargeByKey: state => key => state.charges[key],
    getStatusByKey: state => key => state.statuses[key],
    getProcessNameById: (state, getters) => id => {
      const process = getters.getProcesses({
        id,
      })

      if (process)
        return process[0].name

      return null
    },
    getProjectNameById: state => id => {
      const project = state.projects.filter(project => project.id === id)

      if (project.length)
        return project[0].name

      return null
    },
    getProcesses: state => query => {
      const processes = [].concat.apply([], state.projects.map(project => project.processes))

      if (query) {
        return processes.filter(process => {
          return !(() => {
            const compare = []

            for (let key in query)
              compare.push(query[key] === process[key])

            return compare
          })().includes(false)
        })
      }

      return processes
    },
    getTasks: (state, getters) => query => {
      const tasks = [].concat.apply([], getters.getProcesses().map(proces => proces.tasks))
      
      if (query)
        return tasks.filter(task => {
          return !(() => {
            const compare = []

            for (let key in query) {
              const regexp = new RegExp(/^(date\.+([a-zA-z]+)*)/)
              if (regexp.test(key)) {
                const match = key.match(regexp)
                const time = match[2]
                const val = new Date(task.date[time])

                const start = query[key][0]
                const finish = query[key][1]

                compare.push(val >= start && val <= finish)
              } else {
                switch (key) {
                  case 'like': {
                    const regexp = new RegExp(query[key], 'gi')
                    compare.push(regexp.test(task.subject))
                    break
                  }
                  default: {
                    if (typeof query[key] === 'object')
                      compare.push(
                        query[key].values[
                          task[key].compare === "&" ? "every" : "some"
                        ](item => item === task[key])
                      )
                    else
                      compare.push(query[key] === task[key])
                  }
                }
              }
            }

            return compare
          })().includes(false)
        })

      return tasks
    }
  },
  actions: {
    async fetchProjects({ commit }) {
      // get projects [id, name] from server

      return new Promise(resolve =>
        setTimeout(() => {
          commit('setProjects', PROJECTS)

          resolve(PROJECTS[0])
        }, 1000)
      )
    },
    expendBlocks({ commit, getters }, collapsed) {
      commit('setExpended', collapsed != null ? collapsed : !getters.getExpendBlocks)
    },
    toggleAddTaskVisible({ commit, getters }, visible) {
      commit('setAddTaskVisible', visible ? visible : !getters.getAddTaskVisible)
    },
  },
  modules: {
    task,
    filter,
    project,
  }
})
