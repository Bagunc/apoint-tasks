export default {
  namespaced: true,
  state: () => ({
    date: null,
    status: [],
    process: [],
    search: null,
  }),
  mutations: {
    setData: (state, payload) => {
      if ('date' in payload) state.date = payload.date
      if ('search' in payload) state.search = payload.search
      if ('status' in payload) state.status = payload.status
      if ('process' in payload) state.process = payload.process
    },
    setDate: (state, payload) => state.date = payload,
    setStatus: (state, payload) => state.status = payload,
    setSearch: (state, payload) => state.search = payload,
    setProcess: (state, payload) => state.process = payload
  },
  getters: {
    getDate: state => state.date,
    getStatus: state => state.status,
    getSearch: state => state.search,
    getProcess: state => state.process
  },
  actions: {
    clear: ({ commit }) => commit('setData', {
      date: null,
      status: [],
      process: [],
      search: null,
    })
  },
}