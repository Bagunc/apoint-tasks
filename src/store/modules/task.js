export default {
  namespaced: true,
  state: () => ({
    visible: false,
    data: {
      id: null,
      charge: null,
      status: null,
      client: 126,
      subject: null,
      creator: 'אייפוינט\רם אדרת',
      comment: 'יש לוודא לפני יצירת הצעת מחיר חדשה קיום של נתונים הבאים: קיום לפחות איש קשר פעיל אחד לדירה יש לוודא לפני יצירת הצעת מחיר חדשה קיום של נתונים הבאים: קיום לפחות איש קשר פעיל אחד לדירה יש לוודא לפני יצירת הצעת מחיר חדשה קיום של נתונים הבאים: קיום לפחות איש קשר פעיל אחד לדירה יש לוודא לפני יצירת הצעת מחיר חדשה קיום של נתונים הבאים: קיום לפחות איש קשר פעיל אחד לדירה',
      projectId: null,
      processId: null,
      estimatingHours: null,
      notes: [
        {
          date: "12/10/2020",
          creator: "מוטי אמסלם",
          content: "טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך ",
        },
        {
          date: "12/10/2020",
          creator: "מוטי אמסלם",
          content: "טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך ",
        },
        {
          date: "12/10/2020",
          creator: "מוטי אמסלם",
          content: "טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך ",
        },
        {
          date: "12/10/2020",
          creator: "מוטי אמסלם",
          content: "טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך ",
        },
        {
          date: "12/10/2020",
          creator: "מוטי אמסלם",
          content: "טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך טקסט של הערות/תגובה למסמך ",
        }
      ],
      attachments: [
        {
          date: "20/10/2020",
          creator: "אלכסנדר ג'בוטינסקי",
          filename: "image1.jpg",
          description: "טקסט תיאור של הקובץ",
        },
        {
          date: "20/10/2020",
          creator: "אלכסנדר ג'בוטינסקי",
          filename: "image2.jpg",
          description: "טקסט תיאור של הקובץ",
        }
      ],
      date: {
        start: null,
        finish: null,
        actualEnd: null,
        allegedEnd: null,
        complation: null,
        approvalExecution: null,
      }
    }
  }),
  mutations: {
    setVisible: (state, payload) => {
      if (!payload)
        state.data.id = null

      state.visible = payload
    },
    setData: (state, payload) => state.data = {...state.data, ...payload},
  },
  getters: {
    getData: state => state.data,
    getId: state => state.data.id,
    getVisible: state => state.visible,
    getNotes: state => state.data.notes,
    getAttachments: state => state.data.attachments,
  },
  actions: {
    selectTask: ({ commit, getters, rootGetters }, id) => {
      if (id === getters.getId)
        return commit('setVisible', false)
      
      const task = rootGetters.getTasks({ id })

      if (task.length)
        commit('setData', task[0])
      else
        return null

      if (!getters.getVisible)
        commit('setVisible', true)
    },
    toggleHandler: ({ commit, getters }, value) => commit('setVisible', value !== null ? value : !getters.getVisible)
  }
}