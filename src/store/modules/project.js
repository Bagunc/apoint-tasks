import PROJECTS from '@/static/projects'

import { filterTasks } from '@/utils'

export default {
  namespaced: true,
  state: ({
    id: null,
    name: null,
    processes: [],
  }),
  mutations: {
    setData: (state, { id, name, processes }) => {
      if (id)
        state.id = id

      if (id)
        state.name = name

      if (id)
        state.processes = processes
    }
  },
  getters: {
    getId: state => state.id,
    getProcesses: state => query => {
      const processes = [].concat.apply([], state.processes)

      if (query) {
        return processes.filter(process => {
          return !(() => {
            const compare = []

            for (let key in query) {
              switch (key) {
                case 'like': {
                  const regexp = new RegExp(query[key], 'gi')
                  compare.push(regexp.test(process.name))
                  break
                }
                default: {
                  compare.push(query[key] === process[key])
                }
              }
            }

            return compare
          })().includes(false)
        })
      }

      return processes
    },
    getTasks: (state, getters) => query => {
      const tasks = [].concat.apply([], getters.getProcesses().map(process => process.tasks))

      if (query)
        return filterTasks(tasks, query)

      return tasks
    },
    getFilter: (state, getters) => query => {
      const processTasks = "like" in query ? 
        [].concat.apply([], getters.getProcesses({ like: query.like }).map(process => process.tasks)) :
          []
      const tasks = getters.getTasks(query)
      delete query.like
      const filteredProcessTasks = filterTasks(processTasks, query)

      return [...new Set(
        [
          ...tasks,
          ...filteredProcessTasks
        ]
      )]
    }
  },
  actions: {
    fetchData ({ commit }, id) {
      // get data from server

      return new Promise(resolve => {
        const search = PROJECTS.filter(p => p.id === id)
        const project = search.length ? search[0] : null

        if (project)
          commit('setData', project)

        resolve(project)
      })
    },
  }
}