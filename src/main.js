import { createApp } from 'vue'

import configs from '@/configs'

import PrimeVue from 'primevue/config'

import 'primevue/resources/themes/rhea/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'

import '@/assets/scss/global.scss'

import App from '@/App.vue'
import store from '@/store'
import router from '@/router'

const app = createApp(App)
  .use(store)
  .use(router)
  .use(PrimeVue, configs.primevue)

app.mount('#app')
