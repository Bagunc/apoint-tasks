export const complated = ([
  {
    field: "date.complation", 
    header: "תאריך סיום משימה", 
    sortable: true,
    dataTable: 'date',
  },
  { 
    field: "subject",
    header: "נושא משימה",
    sortable: true,
  },
  { 
    field: "processId",
    header: "תהליך",
    sortable: true,
  },
  { 
    field: "confirmation",
    header: "אישור קבלה",
    sortable: true,
    sortField: 'date.confirmation',
    dataType: 'date',
  },
  { 
    field: "details",
    header: "פרטי משימה"
  }
])

export const awaitExecution = ([
  { 
    field: 'date.start',
    header: 'תאריך יצירה ',
    sortable: true,
    dataTable: 'date',
  },
  { 
    field: 'subject',
    header: 'נושא משימה',
    sortable: true,
  },
  { 
    field: "processId",
    header: "תהליך",
    sortable: true,
  },
  { 
    field: 'estimatingHours',
    header: 'הערכת שעות לביצוע',
    sortable: true,
    dataTable: 'numeric',
  },
  { 
    field: "details",
    header: "פרטי משימה"
  }
])

export const atWork = ([
  {
    field: "date.start",
    header: "תאריך יצירה ",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "subject",
    header: "נושא משימה",
    sortable: true,
  },
  {
    field: "processId",
    header: "תהליך",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "date.finish",
    header: "תאריך מסירה משוער",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "charge",
    header: "אופן חיוב",
    sortable: true,
  },
  {
    field: "estimatingHours",
    header: "הערכת שעות לביצוע",
    sortable: true,
    dataTable: 'numeric',
  },
  {
    field: "details",
    header: "פרטי משימה"
  }
])

export const awaitAcceptance = ([
  {
    field: "date.actualEnd",
    header: "תאריך סיום בפועל",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "subject",
    header: "נושא משימה",
    sortable: true,
  },
  {
    field: "processId",
    header: "תהליך",
    sortable: true,
  },
  {
    field: "confirmation",
    header: "אישור קבלה",
    sortable: true,
    sortField: 'date.confirmation',
    dataType: 'date',
  },
  {
    field: "details",
    header: "פרטי משימה"
  }
])

export const procesColumns = ([
  {
    field: "date.start",
    header: "תאריך יצירה",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "date.approvalExecution",
    header: "תאריך אישור לביצוע",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "subject",
    header: "נושא משימה ",
    sortable: true,
  },
  {
    field: "status",
    header: "סטטוס",
    sortable: true,
  },
  {
    field: "date.allegedEnd",
    header: "תאריך סיום משוער",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "date.actualEnd",
    header: "תאריך סיום בפועל",
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "details",
    header: "פרטי משימה"
  }
])

export const filteringColumns = ([
  {
    field: "date.complation", 
    header: "תאריך יצירה", 
    sortable: true,
    dataTable: 'date',
  },
  {
    field: "date.approvalExecution",
    header: "תאריך אישור לביצוע",
    sortable: true,
    dataTable: 'date',
  },
  { 
    field: "subject",
    header: "נושא משימה ",
    sortable: true,
  },
  { 
    field: "processId",
    header: "תהליך",
    sortable: true,
  },
  { 
    field: "status",
    header: "סטטוס",
    sortable: true,
  },
  { 
    field: "details",
    header: "פרטי משימה"
  }
])

export default {
  atWork,
  complated,
  awaitExecution,
  awaitAcceptance,
  filteringColumns,
}