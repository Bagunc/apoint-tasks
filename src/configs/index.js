import tables from '@/configs/tables'
import primevue from '@/configs/primevue'

export default {
  tables,
  primevue
}
