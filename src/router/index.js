import { createRouter, createWebHistory } from 'vue-router'

import Home from '../views/Home'
import Processes from '../views/Processes'
import Filtering from '../views/Filtering'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    name: 'processes',
    path: '/processes',
    component: Processes
  },
  {
    name: 'filtering',
    path: '/filtering',
    component: Filtering
  }
]

const router = createRouter({
  base: '/',
  routes,
  history: createWebHistory()
})

export default router
