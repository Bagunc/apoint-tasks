export const createRandomId = () => Math.random().toString(36).substr(2)

export const createRandomDate = (start, end) => {
  start = start || new Date(2019, 0, 1)
  end = end || new Date()

  const date = new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  )

  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
}

export const fetchDateField = field => {
  const regexp = new RegExp(/^(date\.+([a-zA-z]+)*)/)
  
  if (regexp.test(field)) {
    const match = field.match(regexp)
    const time = match[2]

    if (time)
      return time

    return false
  }
}

export const filterTasks = (tasks, query) => {
  return tasks.filter(task => {
    return !(() => {
      const compare = []

      for (let key in query) {
        const regexp = new RegExp(/^(date\.+([a-zA-z]+)*)/)
        if (regexp.test(key)) {
          const match = key.match(regexp)
          const time = match[2]
          const val = new Date(task.date[time])

          const start = query[key][0]
          const finish = query[key][1]

          compare.push(val >= start && val <= finish)
        } else {
          switch (key) {
            case 'like': {
              const regexp = new RegExp(query[key], 'gi')
              compare.push(regexp.test(task.subject))
              break
            }
            default: {
              if (typeof query[key] === 'object')
                compare.push(
                  query[key].values[
                    task[key].compare === "&" ? "every" : "some"
                  ](item => item === task[key])
                )
              else
                compare.push(query[key] === task[key])
            }
          }
        }
      }

      return compare
    })().includes(false)
  })
}