module.exports = {
  chainWebpack: config => {
    config.plugin('html')
          .tap(args => {
              args[0].title = "Apoint Tasks Dashboard"
              return args
          })
  },

  css: {
    loaderOptions: {
      scss: {
        prependData: '\n          @import "@/assets/scss/variables.scss";\n        '
      }
    }
  },

  lintOnSave: false
}